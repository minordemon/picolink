import usb_cdc
import json
from time import sleep

serial = usb_cdc.data
print('Listening for data...')

connected = False

while !connected:
    if serial.in_waiting > 0:
        transmitted = serial.readline()
        if transmitted[:-1] == 'hoi':
            sleep(0.1)
            serial.write(b'hoi\n')
            # break

while True:
    if serial.in_waiting > 0:
        transmitted = serial.readline()
        trimmed = transmitted[:-1]
        data = json.loads(trimmed)
        print(data)

# PicoLink

This is an attempt to connect a Raspberry Pi Pico to either Blender or Cinema4d

It can be a connection both ways. Either connecting the virtual to the real world, or controlling the virtual from the real.

## Blender

In Blender the scripts are loaded as 'text' objects inside the Blender file. You can find them in the 'Outliner' in the 'Blender File' view mode.

### Installing modules in Blender

The pip3 installation in Blender is not always properly installed. In Windows there was no pip3 executable.

Find to Blender's python dir run these commands in the interactive console in Blender.

```python
import os
import sys
os.path.dirname(sys.executable)
```

Using the normal terminal (Windows) go to Blender's python directory, and run the pip installer.

```
.\bin\python.exe get-pip.py
```

Once pip is installed you can install python modules with:

```
.\Scripts\pip.exe install {module-name}
```
